#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QVideoWidget>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QCloseEvent>
#include <QTimer>
#include <QResizeEvent>
#include <QWheelEvent>
#include <QFileInfo>
#include <QPropertyAnimation>
#include <QtWinExtras>
#include <QSettings>
#include <QDesktopWidget>
#include <QDropEvent>
#include <iostream>
#include <cmath>

QT_FORWARD_DECLARE_CLASS(QWinTaskbarButton)
QT_FORWARD_DECLARE_CLASS(QWinTaskbarProgress)

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = nullptr);
    ~Form();
    QVideoWidget *vidvid;
    QSettings* rgstr;
    bool full;
    bool sensivity;
    void fullscreen();
    void progress_str(QString&format);
    void file_str(QString &path);
    void simple_info_text(QString &txt);

private:
    Ui::Form *ui;
    QPoint d;
    QPoint g;
    QTimer* t_cur;
    QPropertyAnimation *animation;
    bool trigger;
    QString original_file_name;
    QWinTaskbarButton *win_task_but;
    QWinTaskbarProgress* win_task_prog;


    bool resize_and_move(QSize to_resize);
protected:

    void all_show(bool ani);
    void all_hide();
    bool eventFilter(QObject *watched, QEvent *event);

public slots:

    void t_slot_cur();
    void progress_pos_slot(qint64 pos);
    void progress_dur_slot(qint64 dur);

signals:
    void escaped();
    void fwd();
    void bwd();
    void next();
    void prev();
    void vol_sign(bool);
    void mute(int);
    void space();
    void open_file(QStringList);
    void open_yt(QString);
    void progress_pos_out(qint64);
};

#endif // FORM_H
