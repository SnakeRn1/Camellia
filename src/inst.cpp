#include "inst.h"
#include "ui_inst.h"
#include <iostream>
using namespace std;

inst::inst(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::inst)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);

    QFile file(":/res/inst");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString str = QString::fromLocal8Bit(file.readAll());
    file.close();
    ui->label->setText(str);
}

inst::~inst()
{
    delete ui;
}

void inst::mousePressEvent(QMouseEvent *event)
{
    event->accept();
    this->hide();
}

void inst::leaveEvent(QEvent *event)
{
    event->accept();
    this->hide();
}
