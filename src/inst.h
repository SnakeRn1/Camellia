#ifndef INST_H
#define INST_H

#include <QWidget>
#include <QFile>
#include <QMouseEvent>

namespace Ui {
class inst;
}

class inst : public QWidget
{
    Q_OBJECT

public:
    explicit inst(QWidget *parent = nullptr);
    ~inst();

private:
    Ui::inst *ui;


protected:
    void mousePressEvent(QMouseEvent *event);
    void leaveEvent(QEvent *event);
};

#endif // INST_H
