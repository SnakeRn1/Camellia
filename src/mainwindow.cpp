#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
    setAttribute(Qt::WidgetAttribute::WA_TranslucentBackground);
    setAcceptDrops(true);

    init_list_position = -1;

    shadowEffect = new QGraphicsDropShadowEffect(this);
    shadowEffect->setOffset(0);
    shadowEffect->setBlurRadius(15);
    shadowEffect->setColor(Qt::GlobalColor::yellow);
    ui->widget->setGraphicsEffect(shadowEffect);

    element_shading = new QGraphicsDropShadowEffect(this);
    element_shading->setOffset(0);
    element_shading->setBlurRadius(10);
    element_shading->setColor(Qt::GlobalColor::white);

    ui->win_title->setGraphicsEffect(element_shading);
    ui->button_open->setGraphicsEffect(element_shading);
    ui->button_close->setGraphicsEffect(element_shading);
    ui->button_play->setGraphicsEffect(element_shading);
    ui->button_stop->setGraphicsEffect(element_shading);
    ui->button_pause->setGraphicsEffect(element_shading);
    ui->button_next->setGraphicsEffect(element_shading);
    ui->button_prev->setGraphicsEffect(element_shading);
    ui->camellia->setGraphicsEffect(element_shading);
    ui->tube_button->setGraphicsEffect(element_shading);
    ui->tray_button->setGraphicsEffect(element_shading);
    ui->video_show_button->setGraphicsEffect(element_shading);
    ui->vol_txt->setGraphicsEffect(element_shading);
    ui->file->setGraphicsEffect(element_shading);
    ui->alb->setGraphicsEffect(element_shading);
    ui->aut->setGraphicsEffect(element_shading);
    ui->tra->setGraphicsEffect(element_shading);
    ui->yea->setGraphicsEffect(element_shading);
    ui->files_list->setGraphicsEffect(element_shading);
    ui->repeat_txt->setGraphicsEffect(element_shading);
    ui->repeat->setGraphicsEffect(element_shading);
    ui->top->setGraphicsEffect(element_shading);

    moving_flag = false;
    m_player = new QMediaPlayer(this);

    video_window = new Form;
    inst_window = new inst;
    /* Ниже передача кнопок напрямую к плееру. Помимо этого есть доп. слоты с кнопок */
    connect(ui->button_play, SIGNAL(clicked(bool)), m_player, SLOT(play()));
    connect(ui->button_stop, SIGNAL(clicked(bool)), m_player, SLOT(stop()));
    connect(ui->button_pause, SIGNAL(clicked(bool)), m_player, SLOT(pause()));
    /* С кнопок к неавтоматическим слотам */
    connect(ui->button_next, SIGNAL(clicked(bool)), this, SLOT(open_next_file()));
    connect(ui->button_prev, SIGNAL(clicked(bool)), this, SLOT(open_prev_file()));
    /* Сигнально-слотовое отслеживание динамики состояния плеера для отражения на графике */
    connect(m_player, SIGNAL(positionChanged(qint64)), this, SLOT(position_changed_slot(qint64)));
    connect(m_player, SIGNAL(durationChanged(qint64)), this, SLOT(duration_changed_slot(qint64)));
    connect(m_player, SIGNAL(positionChanged(qint64)), video_window, SLOT(progress_pos_slot(qint64)));
    connect(m_player, SIGNAL(durationChanged(qint64)), video_window, SLOT(progress_dur_slot(qint64)));
    connect(m_player, SIGNAL(metaDataChanged()), this, SLOT(meta_data_changed_slot()));
    connect(m_player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(media_status_changed_slot(QMediaPlayer::MediaStatus)));
    connect(m_player, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(state_changed_slot(QMediaPlayer::State)));
    connect(m_player, SIGNAL(videoAvailableChanged(bool)), this, SLOT(video_available_changed_slot(bool)));
    connect(m_player, SIGNAL(audioAvailableChanged(bool)), this, SLOT(audio_available_changed_slot(bool)));
    /* Передача внешних воздействий от окна видео */
    connect(video_window, SIGNAL(progress_pos_out(qint64)), m_player, SLOT(setPosition(qint64)), Qt::QueuedConnection);
    connect(video_window, SIGNAL(escaped()), this, SLOT(video_window_escaped()), Qt::QueuedConnection);
    connect(video_window, SIGNAL(fwd()), this, SLOT(fwd()), Qt::QueuedConnection);
    connect(video_window, SIGNAL(bwd()), this, SLOT(bwd()), Qt::QueuedConnection);
    connect(video_window, SIGNAL(next()), this, SLOT(open_next_file()), Qt::QueuedConnection);
    connect(video_window, SIGNAL(prev()), this, SLOT(open_prev_file()), Qt::QueuedConnection);
    connect(video_window, SIGNAL(vol_sign(bool)), this, SLOT(vol_slot(bool)), Qt::QueuedConnection);
    connect(video_window, SIGNAL(mute(int)), this, SLOT(on_vol_valueChanged(int)), Qt::QueuedConnection);
    connect(video_window, SIGNAL(space()), this, SLOT(space()), Qt::QueuedConnection);
    connect(video_window, SIGNAL(open_file(QStringList)), this, SLOT(open_new_file(QStringList)), Qt::QueuedConnection);
    connect(video_window, SIGNAL(open_yt(QString)), this, SLOT(add_yt_link(QString)), Qt::QueuedConnection);

    ui->progress->setMaximum(0);
    duration_str = "00:00";
    position_str = "00:00";
    QString stat = position_str + "/" + duration_str;
    ui->progress->setFormat(stat);
    ui->video_show_button->hide();
    m_player->setVideoOutput(video_window->vidvid);
    ui->progress->installEventFilter(this);
    ui->camellia->installEventFilter(this);
    this->installEventFilter(this);

    /* Создание иконки трея и пунктов меню к ней */
    trayIcon = new QSystemTrayIcon(this);
    QMenu * menu = new QMenu();

    QIcon open_icon (":/res/open");
    QIcon close_icon (":/res/close");
    QIcon tube_icon (":/res/tube");

    QAction * open = new QAction(open_icon, "Открыть", this);
    QAction * tube = new QAction(tube_icon, "YouTube", this);
    QAction * quit = new QAction(close_icon, "Выход", this);

    menu->addAction(open);
    menu->addAction(tube);
    menu->addAction(quit);

    connect(open, SIGNAL(triggered()), this, SLOT(on_button_open_clicked()));
    connect(quit, SIGNAL(triggered()), this, SLOT(on_button_close_clicked()));
    connect(tube, SIGNAL(triggered()), this, SLOT(on_tube_button_clicked()));

    trayIcon->setContextMenu(menu);
    QIcon tr_ic(":/res/play");
    trayIcon->setIcon(tr_ic);
    trayIcon->setToolTip("Camellia Media Player");
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayIconActivated(QSystemTrayIcon::ActivationReason)));
    ui->top->setChecked(video_window->rgstr->value("player_always_on_top").toBool());
    if (video_window->rgstr->value("player_window_hidden").toBool()) on_tray_button_clicked();
    else show();
    QPoint l = video_window->rgstr->value("player_window_location_point").toPoint();
    if(QApplication::desktop()->geometry().contains(l)) move(l);
    int vol = video_window->rgstr->value("volume").toInt();
    if(vol>=10) on_vol_valueChanged(vol);
    tm1 = new QTimer;
    connect(tm1,SIGNAL(timeout()),this,SLOT(open_by_timer()));
    tm1->start(1000);
    tm2 = new QTimer;
    connect(tm2,SIGNAL(timeout()),this,SLOT(open_by_timer2()));
    tm2->start(200);
}

void MainWindow::open_by_timer()
{
    QStringList l = video_window->rgstr->value("to_open").toStringList();
    if(!l.empty())
    {
        open_new_file(l);
        video_window->rgstr->setValue("to_open",QStringList());
    }

}
void MainWindow::open_by_timer2()
{
    if(!yt_str.isEmpty())
    {
        open_yt_link(yt_str);
        last_opened_yt_link = yt_str;
        yt_str.clear();
    }
}

MainWindow::~MainWindow()
{
    delete tm1;
    delete shadowEffect;
    delete element_shading;
    delete trayIcon;
    delete inst_window;
    delete m_player;
    delete video_window;
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    tm1->stop();
    if ((m_player->state() == QMediaPlayer::State::PlayingState)||(m_player->state() == QMediaPlayer::State::PausedState))
    {
        video_window->rgstr->setValue("timing_memory_file",file);
        video_window->rgstr->setValue("timing_memory_position",m_player->position());
    }
    else
    {
        video_window->rgstr->setValue("timing_memory_file","");
        video_window->rgstr->setValue("timing_memory_position",qlonglong(0));
    }
    if(list.size() == 1)
    {
        if(list.at(0).contains("https")) list.clear();
    }
    video_window->rgstr->setValue("last_files_list",list);
    video_window->rgstr->setValue("last_files_index",index);
    video_window->rgstr->setValue("last_opened_yt_link",last_opened_yt_link);
    video_window->rgstr->setValue("player_window_location_point",pos());
    video_window->rgstr->setValue("volume",m_player->volume());
    m_player->stop();
    inst_window->close();

    if(this->isHidden())
        video_window->rgstr->setValue("player_window_hidden",true);
    else video_window->rgstr->setValue("player_window_hidden",false);
    video_window->rgstr->setValue("player_always_on_top",ui->top->isChecked());
    video_window->close();
    file_addr_proc.waitForFinished(10000);
    file_name_proc.waitForFinished(10000);
    event->accept();
    emit exited();
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if((event->buttons() == Qt::LeftButton)&&moving_flag)
    {
        QPoint to_move = event->globalPos()-d;
        QPoint br = geometry().topLeft() - geometry().bottomRight() + screen()->availableGeometry().bottomRight();
        QPoint tl = screen()->availableGeometry().topLeft();
        tl.setY(tl.y()+40);
        QPoint delta_br = to_move-br;
        QPoint delta_tl = to_move-tl;
        if(abs(delta_br.x()) <= 40) to_move.setX(br.x()+roundf(delta_br.x()*fabs(delta_br.x()/100.0f)));
        if(abs(delta_br.y()) <= 40) to_move.setY(br.y()+roundf(delta_br.y()*fabs(delta_br.y()/100.0f)));
        if(abs(delta_tl.x()) <= 40) to_move.setX(tl.x()+roundf(delta_tl.x()*fabs(delta_tl.x()/100.0f)));
        if(abs(delta_tl.y()) <= 40) to_move.setY(tl.y()+roundf(delta_tl.y()*fabs(delta_tl.y()/100.0f)));
        move(to_move);
    }
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == ui->camellia)
    {
        if(event->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->buttons() == Qt::LeftButton)
            {
                inst_window->move(e->globalX()-inst_window->width()/2,e->globalY()-inst_window->height()/2);
                inst_window->show();
                return true;
            }
        }
    }
    if (watched == ui->progress)
    {
        if (m_player->mediaStatus() == QMediaPlayer::NoMedia) return false;
        if(event->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->button() == Qt::LeftButton)
            {
                float max = static_cast<float>(ui->progress->maximum());
                float width = static_cast<float>(ui->progress->width());
                float now = static_cast<float>(e->x());
                float ms = now*(max/width);
                qint64 pos_now_ms = llround(ms);
                m_player->setPosition(pos_now_ms);
                return true;
            }
        }
    }
    if (watched == this)
    {
        if(event->type() == QEvent::DragEnter)
        {
            QDragEnterEvent* e = static_cast<QDragEnterEvent*>(event);
            if(e->mimeData()->hasUrls()) e->acceptProposedAction();
        }
        if(event->type() == QEvent::Drop)
        {
            QDropEvent* e = static_cast<QDropEvent*>(event);
            QStringList to_open;
            if(e->mimeData()->urls().length()==1)
            {
                if (e->mimeData()->text().contains("youtube")) add_yt_link(e->mimeData()->text());
                else
                {
                    to_open.push_back(e->mimeData()->urls().first().toLocalFile());
                    open_new_file(to_open);
                }
            }
            else
            {
                foreach (QUrl u, e->mimeData()->urls()) to_open.push_back(u.toLocalFile());
                open_new_file(to_open);
            }
        }
        if(event->type() == QEvent::KeyPress)
        {
            QKeyEvent* e = static_cast<QKeyEvent*>(event);
            if(e->key() == Qt::Key_Escape) this->close();
            if(e->key() == Qt::Key_Space) space();
            if((e->key() == Qt::Key_Right)||(e->key() == 46)||(e->key() == 1070)) open_next_file();
            if((e->key() == Qt::Key_Left)||(e->key() == 44)||(e->key() == 1041)) open_prev_file();
            if(e->key() == Qt::Key_Up) vol_slot(true);
            if(e->key() == Qt::Key_Down) vol_slot(false);
            if((e->key() == Qt::Key_F)||(e->key() == 1040)) {if(m_player->isVideoAvailable()) video_window->fullscreen();}
            if((e->key() == Qt::Key_O)||(e->key() == 1065)) open_new_file(QStringList());
            if((e->key() == Qt::Key_M)||(e->key() == 1068)) on_vol_valueChanged(501);
            if((e->key() == Qt::Key_Y)||(e->key() == 1053)) open_yt_link(QString());
            return true;
        }
        if(event->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->buttons() == Qt::LeftButton)
            {
                moving_flag = true;
                d = e->pos();
            }
            if(e->buttons() == Qt::RightButton)
            {
                //if(ui->top->isChecked())ui->top->setChecked(false);
                //else
                on_tray_button_clicked();
            }
        }
        if(event->type() == QEvent::MouseButtonRelease)
        {
            moving_flag = false;
        }
    }
    return false;
}

void MainWindow::fwd()
{
    qint64 dst = pos_now_ms+5000;
    if(dst<dur_now_ms) m_player->setPosition(dst);
}

void MainWindow::bwd()
{
    qint64 dst = pos_now_ms-5000;
    if(dst>0) m_player->setPosition(dst);
    if(dst<=0) m_player->setPosition(0);
}

void MainWindow::vol_slot(bool up)
{
    int vol_now = m_player->volume();
    if(up)
    {
        vol_now +=10;
        if(vol_now>100) vol_now = 100;
    }
    else
    {
        vol_now -=10;
        if(vol_now<0) vol_now = 0;
    }
    on_vol_valueChanged(vol_now);
}

void MainWindow::space()
{
    if(m_player->state() == QMediaPlayer::PausedState) m_player->play();
    else if(m_player->state() == QMediaPlayer::PlayingState) m_player->pause();
}

void MainWindow::position_changed_slot(qint64 pos_now)
{
    pos_now_ms = pos_now;
    int now = static_cast<int>(pos_now);
    ui->progress->setValue(now);
    position_str = getTimingQStr(now);
    QString stat = position_str + "/" + duration_str;
    ui->progress->setFormat(stat);
    video_window->progress_str(stat);
}

void MainWindow::duration_changed_slot(qint64 dura_now)
{
    dur_now_ms = dura_now;
    int now = static_cast<int>(dura_now);
    ui->progress->setMaximum(now);
    ui->progress->setValue(0);
    duration_str = getTimingQStr(now);
}

QString MainWindow::getTimingQStr(int msf)
{
    int ms = msf%1000;
    int scf = msf/1000;
    if (scf < 60) return QString("%1.%2s").arg(scf, 2, 10, QChar('0')).arg(ms, 3, 10, QChar('0'));
    int sc = scf%60;
    int mnf = scf/60;
    if (mnf < 60) return QString("%1:%2").arg(mnf, 2, 10, QChar('0')).arg(sc, 2, 10, QChar('0'));
    int mn = mnf%60;
    int hsf = mnf/60;
    if (hsf < 24) return QString("%1:%2:%3").arg(hsf, 2, 10, QChar('0')).arg(mn, 2, 10, QChar('0')).arg(sc, 2, 10, QChar('0'));
    int hs = hsf%24;
    int dsf = hsf/24;
    return QString("%1d:%2h:%3m.%4s.%5ms").arg(dsf, 1, 10, QChar('0')).arg(hs, 2, 10, QChar('0')).arg(mn, 2, 10, QChar('0')).arg(sc, 2, 10, QChar('0')).arg(ms, 3, 10, QChar('0'));
}

void MainWindow::add_yt_link(QString input_str)
{
    yt_str = input_str;
    QString tm_str("Связь с сервером Youtube...");
    video_window->simple_info_text(tm_str);
    ui->file->setText(tm_str);
}

void MainWindow::open_yt_link(QString input_str)
{
    bool bOk = false;
    if (input_str.isEmpty())
    {
        input_str = QInputDialog::getText(this,"Введите адрес видео","https://www.youtube.com/watch?***",QLineEdit::Normal,"",&bOk,Qt::Tool);
        if(bOk) add_yt_link(input_str);
        return;
    }
    else bOk = true;
    if (bOk)
    {
        if(input_str.contains("https")&&input_str.contains("youtube")&&input_str.contains("watch"))
        {
            file_addr_proc.waitForFinished(10000);
            file_name_proc.waitForFinished(10000);
            file_addr_proc.start( "youtube-dl.exe", QStringList() << "-g" << "--no-playlist" << "-f best" << input_str );
            file_name_proc.start( "youtube-dl.exe", QStringList() << "-e" << input_str );
            bOk = file_addr_proc.waitForReadyRead(10000);
            if(bOk)
            {
                QString temp = file_addr_proc.readAllStandardOutput();
                if(!temp.isEmpty())
                {
                    index = 0;
                    list.clear();
                    list.push_back(temp);
                    ui->files_list->clear();
                    yt_file.clear();
                    file_name_proc.waitForReadyRead(10000);
                    yt_file = QString::fromLocal8Bit(file_name_proc.readAllStandardOutput());
                    if(yt_file.isEmpty()) yt_file = "youtube_link";
                    ui->files_list->addItem(yt_file);
                    try_to_open_file_by_index();
                }
                else QMessageBox::about(this,"Информация","Со стороны сайта была получена пустая ссылка");
            }
            else QMessageBox::about(this,"Информация","Ссылка не была определена");
        }
        else QMessageBox::about(this,"Информация","Адрес не содержит ссылки на видео");
    }
    else QMessageBox::about(this,"Информация","Адрес введен неверно");
}

void MainWindow::open_new_file(QStringList to_open)
{
    if (!to_open.empty())
    {
        list.clear();
        ui->files_list->clear();
        foreach (QString u, to_open)
        {
            QDir dir(u);
            if(dir.exists())
            {
                dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
                dir.setSorting(QDir::Name);
                QStringList temp_open = dir.entryList();
                foreach (QString f,temp_open) list.push_back(dir.absoluteFilePath(f));
            }
            else list.push_back(u);
        }
        for(int ci = 0;ci<list.size();ci++)
        {
            if  (   !list.at(ci).contains(".mp3",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".mkv",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".flac",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".aac",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".ac3",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".m4a",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".mp4",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".avi",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".mov",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".webm",Qt::CaseInsensitive)&&
                    !list.at(ci).contains(".ogg",Qt::CaseInsensitive))
            {
                list.removeAt(ci);
                ci--;
            }
            else
            {
                list[ci] = QDir::fromNativeSeparators(list.at(ci));
                ui->files_list->addItem(QFileInfo(list.value(ci)).fileName());
            }
        }
        if (init_list_position != -1)
        {
            index = init_list_position;
            init_list_position = -1;
        }
        else index = 0;
        try_to_open_file_by_index();
    }
    else
    {
        bool ch = ui->top->isChecked();
        if(!ch)
        {
            this->setWindowFlag(Qt::WindowType::WindowStaysOnTopHint,true);
        }
        QStringList opened;
        opened = QFileDialog::getOpenFileNames(this,"Open files",QString(),"Audio|Video Files (*.mp3 *.mkv *.flac *.aac *.ac3 *.m4a *.mp4 *.avi *.mov *.webm *.ogg)");
        if(!ch)
        {
            setWindowFlag(Qt::WindowType::WindowStaysOnTopHint,false);
            show();
            if(trayIcon->isVisible())this->hide();
        }
        if(!opened.empty())
        {
            list = opened;
            ui->files_list->clear();
            for(int ci = 0;ci<opened.size();ci++) ui->files_list->addItem(QFileInfo(opened.value(ci)).fileName());
            index = 0;
            try_to_open_file_by_index();
        }
    }
    if(list.empty())
    {
        show();
        trayIcon->hide();
        activateWindow();
        m_player->stop();
        m_player->setMedia(nullptr);
        ui->button_play->setDisabled(true);
        ui->file->setText("Откройте файл");
    }
}

void MainWindow::open_prev_file()
{
    index--;
    if((index>=0)&&(index<list.size())&&(!list.empty())) try_to_open_file_by_index();
    else index ++;
}

void MainWindow::open_next_file()
{
    index ++;
    if((index>=0)&&(index<list.size())&&(!list.empty())) try_to_open_file_by_index();
    else index --;
}

void MainWindow::open_by_index()
{
    if((index>=0)&&(index<list.size())&&(!list.empty()))
    {
        m_player->stop();
        file = list.at(index);
        if(!file.contains("http")) ui->file->setText(file);
        else ui->file->setText(yt_file);
        m_player->setMedia(QUrl::fromLocalFile(file));
        m_player->play();
        if(m_player->isAvailable())
        {
            if((video_window->rgstr->value("timing_memory_file").toString() == file)
                    ||((video_window->rgstr->value("last_opened_yt_link").toString() == yt_str)&&(!yt_str.isEmpty())))
            {
                qint64 p = video_window->rgstr->value("timing_memory_position").toLongLong();
                if(p>0)
                {
                    QMessageBox q(this);
                    q.setInformativeText("Продолжить воспроизведение с предыдущей позиции ["+ getTimingQStr(p)+"] ?");
                    q.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
                    q.setDefaultButton(QMessageBox::Yes);
                    q.setWindowModality(Qt::WindowModality::ApplicationModal);
                    q.setWindowFlag(Qt::WindowType::WindowStaysOnTopHint);
                    q.setWindowFlag(Qt::WindowType::Sheet);
                    q.setIcon(QMessageBox::Question);
                    if(q.exec() == QMessageBox::Yes) m_player->setPosition(p);
                }
                video_window->rgstr->setValue("timing_memory_file","");
                video_window->rgstr->setValue("timing_memory_position",qlonglong(0));
            }
        }
    }
}

void MainWindow::check_moving()
{
    int size = list.size();
    if(size>1)
    {
        if(index<1) ui->button_prev->setEnabled(false);
        else ui->button_prev->setEnabled(true);
        if(index>(size-2)) ui->button_next->setEnabled(false);
        else ui->button_next->setEnabled(true);
    }
    else
    {
        ui->button_next->setEnabled(false);
        ui->button_prev->setEnabled(false);
    }
}

void MainWindow::on_vol_valueChanged(int value)
{
    if((value == 500)&&(m_player->volume() == 0))
    {
        ui->vol->setValue(muted_vol);
        return;
    }
    if(value == 501)
    {
        if(m_player->volume() == 0) ui->vol->setValue(muted_vol);
        else ui->vol->setValue(0);
        return;
    }
    if((value>=0)&&(value<=100))
    {
        if(ui->vol->value() != value) ui->vol->setValue(value);
        else
        {
            if(value != 0)
            {
                m_player->setVolume(value);
                QString send = "vol: " + QString::number(value) + "%";
                ui->vol_txt->setText(send);
                video_window->simple_info_text(send);
            }
            else
            {
                if(m_player->volume() != 0) muted_vol = m_player->volume();
                m_player->setVolume(value);
                QString send = "БЕЗ ЗВУКА";
                ui->vol_txt->setText(send);
                video_window->simple_info_text(send);
            }
        }
    }
}

void MainWindow::meta_data_changed_slot()
{
    QString aut = m_player->metaData(QMediaMetaData::ContributingArtist).toString();
    QString tra = m_player->metaData(QMediaMetaData::Title).toString();
    QString alb = m_player->metaData(QMediaMetaData::AlbumTitle).toString();
    QString yea = m_player->metaData(QMediaMetaData::Year).toString();
    if(!aut.isEmpty())ui->aut->setText("Исполнитель:\t"   +aut);
    else ui->aut->clear();
    if(!aut.isEmpty()) ui->tra->setText("Название:\t"   +tra);
    else ui->tra->clear();
    if(!aut.isEmpty()) ui->alb->setText("Альбом:\t"+alb);
    else ui->alb->clear();
    if(!aut.isEmpty()) ui->yea->setText("Год:\t" +yea);
    else ui->yea->clear();
}

void MainWindow::state_changed_slot(QMediaPlayer::State state)
{
    if(state == QMediaPlayer::State::StoppedState)
    {
        ui->button_play->setEnabled(true);
        ui->button_pause->setEnabled(false);
        ui->button_stop->setEnabled(false);
        if(m_player->isVideoAvailable())ui->video_show_button->setHidden(false);
    }
    if(state == QMediaPlayer::State::PlayingState)
    {
        ui->button_play->setEnabled(false);
        ui->button_pause->setEnabled(true);
        ui->button_stop->setEnabled(true);
        if (!file.contains("http")) video_window->file_str(file);
        else video_window->file_str(yt_file);
    }
    if(state == QMediaPlayer::State::PausedState)
    {
        ui->button_play->setEnabled(true);
        ui->button_pause->setEnabled(false);
        ui->button_stop->setEnabled(true);
        QString send("ПАУЗА");
        video_window->file_str(send);
    }
}

void MainWindow::media_status_changed_slot(QMediaPlayer::MediaStatus status)
{
    if (status == QMediaPlayer::MediaStatus::EndOfMedia)
    {
        int ind = ui->repeat->currentIndex();
        if(ind == 0) // one
        {
            m_player->play();
        }
        if(ind == 1) //all
        {
            if(index == (list.size()-1)) index = -1;
            open_next_file();
        }
        if(ind == 2) //no
        {
            if(index != (list.size()-1)) open_next_file();
            else on_button_stop_clicked();
        }
    }
}

void MainWindow::video_available_changed_slot(bool ava)
{
    if(ava)
    {
        video_window->show();
    }
    else
    {
        ui->video_show_button->hide();
        //video_window->hide();
    }
}

void MainWindow::audio_available_changed_slot(bool ava)
{
    if(ava&&(!m_player->isVideoAvailable())) video_window->hide();
}

void MainWindow::video_window_escaped()
{
    if(video_window->full)
    {
        video_window->full = false;
        video_window->showNormal();
    }
    else
    {
        ui->video_show_button->show();
        m_player->pause();
        video_window->hide();
    }
}

void MainWindow::on_video_show_button_clicked()
{
    video_window->show();
    ui->video_show_button->hide();
}

void MainWindow::on_button_play_clicked()
{
    if(m_player->isVideoAvailable())
    {
        on_video_show_button_clicked();
    }
}

void MainWindow::on_button_stop_clicked()
{
    video_window->showNormal();
    video_window->full = false;
    video_window->hide();
}

void MainWindow::on_button_close_clicked()
{
    this->close();
}

void MainWindow::on_button_open_clicked()
{
    open_new_file(QStringList());
}

void MainWindow::on_tube_button_clicked()
{
    open_yt_link(QString());
}

void MainWindow::on_top_stateChanged(int arg1)
{
    if(arg1) this->setWindowFlag(Qt::WindowStaysOnTopHint,true);
    else this->setWindowFlag(Qt::WindowStaysOnTopHint,false);
    show();
}

void MainWindow::on_tray_button_clicked()
{
    this->hide();
    trayIcon->show();
    trayIcon->showMessage("Внимание!","Приложение Camellia Media Player свёрнуто в трей, чтобы открыть его, щелкните по значку.");
}

void MainWindow::trayIconActivated(QSystemTrayIcon::ActivationReason re)
{
    if(re != QSystemTrayIcon::ActivationReason::Context)
    {
        if(m_player->isVideoAvailable()&&video_window->isHidden())
        {
            on_video_show_button_clicked();
            m_player->play();
        }
        else
        {
            this->show();
            trayIcon->hide();
            this->activateWindow();
        }
    }
}

void MainWindow::on_files_list_itemClicked(QListWidgetItem *item)
{
    int ind = ui->files_list->row(item);
    if ((index != ind)&&(ind!=-1)) //currentRow == -1 скорее всего при обновлении списка
    {
        index = ind;
        try_to_open_file_by_index();
    }
}

void MainWindow::try_to_open_file_by_index()
{
    open_by_index();
    check_moving();
    if(index != ui->files_list->currentRow()) ui->files_list->setCurrentRow(index);
}

void MainWindow::on_rate_currentTextChanged(const QString &arg1)
{
    m_player->setPlaybackRate(arg1.toDouble());
}

void MainWindow::on_files_list_customContextMenuRequested(const QPoint &pos)
{
    QMenu * menu = new QMenu(this);
    QIcon ico(":/res/close");
    QAction * deleteDevice = new QAction(ico, "Удалить", this);
    connect(deleteDevice, SIGNAL(triggered()), this, SLOT(slotRemoveRecord())); // Обработчик удаления записи
    menu->addAction(deleteDevice);
    menu->popup(ui->files_list->viewport()->mapToGlobal(pos));
}

void MainWindow::slotRemoveRecord()
{
    int current_index = ui->files_list->currentRow();
    list.removeAt(current_index);
    ui->files_list->clear();
    foreach (QFileInfo i,list) ui->files_list->addItem(i.fileName());
    index = list.indexOf(file);
    ui->files_list->setCurrentRow(index);
    check_moving();
}
