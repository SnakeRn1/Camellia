#include "form.h"
#include "ui_form.h"

using namespace std;

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);

    full = false;
    trigger = false;
    sensivity = true;
    setAcceptDrops(true);

    animation = new QPropertyAnimation(ui->progressBar, "size",this);
    animation->setDuration(50);
    animation->setEasingCurve(QEasingCurve::Type::OutExpo);

    this->setMaximumSize(QApplication::primaryScreen()->size().height()/9*16,QApplication::primaryScreen()->size().height());

    vidvid = new QVideoWidget(this);
    vidvid->setMinimumSize(this->minimumSize());
    vidvid->setMaximumSize(this->maximumSize());
    vidvid->resize(this->size());
    vidvid->setMouseTracking(true);
    vidvid->installEventFilter(this);
    ui->progressBar->setMouseTracking(true);
    ui->progressBar->installEventFilter(this);
    this->installEventFilter(this);

    all_hide();

    t_cur = new QTimer(this);
    connect(t_cur,SIGNAL(timeout()),this,SLOT(t_slot_cur()));

/* Следующая ветка относится к прогресс-бару на значке в панели задач */

    if (QtWin::isCompositionEnabled()) QtWin::extendFrameIntoClientArea(this, 0, 0, 0, 0);
    else QtWin::resetExtendedFrame(this);

    win_task_but = new QWinTaskbarButton(this);
    win_task_but->setWindow(windowHandle());
    win_task_prog = win_task_but->progress();
    win_task_prog->show();

    rgstr = new QSettings("SnakeR Soft","Camellia Media Player");

    QRect r = rgstr->value("video_window_geometry_rect").toRect();
    if(QApplication::desktop()->geometry().contains(r)) this->setGeometry(r);

}

Form::~Form()
{
    if(!full) rgstr->setValue("video_window_geometry_rect",geometry());
    delete rgstr;
    delete win_task_prog;
    delete animation;
    delete t_cur;
    delete vidvid;
    delete ui;
}

void Form::all_show(bool ani)
{
    if(!trigger)
    {
        trigger = true;
        this->setCursor(Qt::ArrowCursor);
        ui->filename->raise();
        ui->progressBar->raise();
        if(ani)
        {
            animation->setStartValue(QSize(50,ui->progressBar->size().height()));
            animation->setEndValue(ui->progressBar->size());
            animation->start();
        }
    }
    t_cur->start(2000);
}

void Form::all_hide()
{
    ui->filename->lower();
    ui->progressBar->lower();
    ui->filename->setText(original_file_name);
}

void Form::t_slot_cur()
{
    t_cur->stop();
    if(full) this->setCursor(Qt::BlankCursor);
    all_hide();
    trigger = false;
}

void Form::progress_pos_slot(qint64 pos)
{
    int val = static_cast<int>(pos);
    ui->progressBar->setValue(val);
    win_task_prog->setValue(val);
}

void Form::progress_dur_slot(qint64 dur)
{
    int max = static_cast<int>(dur);
    ui->progressBar->setMaximum(max);
    win_task_prog->setMaximum(max);
}

bool Form::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == vidvid)
    {
        if(event->type() == QEvent::MouseMove)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(!full&&(e->buttons() == Qt::LeftButton))
            {
                QPoint for_moving = e->globalPos() - d;
                QPoint on_display = for_moving - screen()->geometry().topLeft();
                QPoint moving_border(screen()->size().width() - size().width(),screen()->size().height() - size().height());
                QPoint ava_border(screen()->availableSize().width() - size().width(),screen()->availableSize().height() - size().height());
                int deviation = 20;
                if(abs(on_display.x())<=deviation) for_moving.setX(screen()->geometry().x());
                if(abs(on_display.y())<=deviation) for_moving.setY(screen()->geometry().y());
                if(abs(on_display.x()-ava_border.x())<=deviation) for_moving.setX(ava_border.x());
                if(abs(on_display.y()-ava_border.y())<=deviation) for_moving.setY(ava_border.y());
                if(abs(on_display.x()-moving_border.x())<=deviation) for_moving.setX(moving_border.x()+screen()->geometry().x());
                if(abs(on_display.y()-moving_border.y())<=deviation) for_moving.setY(moving_border.y()+screen()->geometry().y());

                move(for_moving);
            }
            if(e->buttons() == Qt::NoButton) all_show(true);
            if(full&&(e->buttons() == Qt::LeftButton)&&sensivity)
            {
                if(e->y() == (size().height()-1))
                {
                    emit mute(0);
                    sensivity = false;
                }
                if(e->y() == 0)
                {
                    emit mute(500);
                    sensivity = false;
                }
                if(e->x() == 0)
                {
                    emit prev();
                    sensivity = false;
                }
                if(e->x() == (size().width()-1))
                {
                    emit next();
                    sensivity = false;
                }
            }
        }
        if(event->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->buttons() == Qt::LeftButton)
            {
                if(!full) d = e->pos();
                g=e->globalPos();
            }
        }
        if(event->type() == QEvent::MouseButtonRelease)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(         (abs(e->globalX()-g.x()) < 5)
                    &&  (abs(e->globalY()-g.y()) < 5))
                emit space();
            sensivity = true;
        }
    }
    if (watched == ui->progressBar)
    {
        if(event->type() == QEvent::MouseMove)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->buttons() == Qt::NoButton) all_show(true);
        }
        if(event->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->button() == Qt::LeftButton)
            {
                float max = static_cast<double>(ui->progressBar->maximum());
                float width = static_cast<double>(ui->progressBar->width());
                float now = static_cast<double>(e->x());
                float ms = now*(max/width);
                qint64 pos_now_ms = llround(ms);
                emit progress_pos_out(pos_now_ms);
            }
        }
        if(event->type() == QEvent::MouseButtonDblClick) return true;
    }
    if (watched == this)
    {
        if(event->type() == QEvent::DragEnter)
        {
            QDragEnterEvent* e = static_cast<QDragEnterEvent*>(event);
            if(e->mimeData()->hasFormat("text/uri-list")) e->acceptProposedAction();
        }
        if(event->type() == QEvent::Drop)
        {
            QDropEvent* e = static_cast<QDropEvent*>(event);
            QStringList to_open;
            if(e->mimeData()->urls().length()==1)
            {
                if (e->mimeData()->text().contains("youtube")) emit open_yt(e->mimeData()->text());
                else
                {
                    to_open.push_back(e->mimeData()->urls().first().toLocalFile());
                    emit open_file(to_open);
                }
            }
            else
            {
                foreach (QUrl u, e->mimeData()->urls()) to_open.push_back(u.toLocalFile());
                emit open_file(to_open);
            }
        }
        if(event->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->buttons() == Qt::RightButton) emit escaped();
            if(e->buttons() == Qt::MiddleButton) emit open_file(QStringList());
        }
        if(event->type() == QEvent::KeyPress)
        {
            QKeyEvent* e = static_cast<QKeyEvent*>(event);
            if(e->key() == Qt::Key_Escape) emit escaped();
            if(e->key() == Qt::Key_Right) emit fwd();
            if(e->key() == Qt::Key_Left) emit bwd();
            if(e->key() == Qt::Key_Up) emit vol_sign(true);
            if(e->key() == Qt::Key_Down) emit vol_sign(false);
            if(e->key() == Qt::Key_Space) emit space();
            if((e->key() == Qt::Key_F)||(e->key() == 1040)) fullscreen();
            if((e->key() == Qt::Key_O)||(e->key() == 1065)) emit open_file(QStringList());
            if((e->key() == Qt::Key_M)||(e->key() == 1068)) emit mute(501);
            if((e->key() == Qt::Key_Y)||(e->key() == 1053)) emit open_yt(QString());
            if((e->key() == 44)||(e->key() == 1041)) emit prev();
            if((e->key() == 46)||(e->key() == 1070)) emit next();
            if(e->key() == Qt::Key_1) resize_and_move(QSize(256,144));
            if(e->key() == Qt::Key_2) resize_and_move(QSize(320,180));
            if(e->key() == Qt::Key_3) resize_and_move(QSize(480,270));
            if(e->key() == Qt::Key_4) resize_and_move(QSize(640,360));
            if(e->key() == Qt::Key_5) resize_and_move(QSize(800,450));
            if(e->key() == Qt::Key_6) resize_and_move(QSize(960,540));
            if(e->key() == Qt::Key_7) resize_and_move(QSize(1280,720));
            if(e->key() == Qt::Key_8) resize_and_move(QSize(1600,900));
            if(e->key() == Qt::Key_9) resize_and_move(QSize(1920,1080));
            if(e->key() == Qt::Key_0) resize_and_move(vidvid->sizeHint());
        }
        if(event->type() == QEvent::Wheel)
        {
            QWheelEvent* e = static_cast<QWheelEvent*>(event);
            int delta = e->delta()/120;
            if(!full)
            {
                int new_he = (size().height()+18*delta)/18;
                resize_and_move(QSize(new_he*32,new_he*18));
            }
            else
            {
                if(delta>0) emit vol_sign(true);
                if(delta<0) emit vol_sign(false);
            }
        }
        if(event->type() == QEvent::Resize)
        {
            QResizeEvent* e = static_cast<QResizeEvent*>(event);
            vidvid->resize(e->size());
        }
        if(event->type() == QEvent::MouseButtonDblClick)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->buttons() == Qt::LeftButton) fullscreen();
        }
    }
    return false;
}

bool Form::resize_and_move(QSize to_resize)
{
    if(full) return false;
    if(         (to_resize.width()<=screen()->size().width())
              &&(to_resize.height()<=screen()->size().height())
              &&(to_resize.width()>=minimumWidth())
              &&(to_resize.height()>=minimumHeight()))
    {
        if(animation->state() == QAbstractAnimation::State::Running) animation->stop();
        int delta = to_resize.height() - size().height();
        bool s_g = false;
        QPoint to_move(pos());
        if(delta>0)
        {
            QPoint to_test = to_move - screen()->availableGeometry().topLeft();
            if(to_test.x()+to_resize.width()>screen()->availableSize().width())
            {
                s_g = true;
                to_move.setX(screen()->availableGeometry().bottomRight().x()-to_resize.width()+1);
            }
            if(to_test.y()+to_resize.height()>screen()->availableSize().height())
            {
                s_g = true;
                to_move.setY(screen()->availableGeometry().bottomRight().y()-to_resize.height()+1);
            }

            if(to_move.x()<screen()->geometry().x())
            {
                s_g = true;
                to_move.setX(screen()->geometry().x());
            }
            if(to_move.y()<screen()->geometry().y())
            {
                s_g = true;
                to_move.setY(screen()->geometry().y());
            }
        }
        if(delta<0)
        {
            QPoint delta_br(screen()->availableGeometry().bottomRight()-geometry().bottomRight());
            if(delta_br.x()<=0)
            {
                s_g = true;
                to_move.setX(screen()->availableGeometry().bottomRight().x()-to_resize.width()+1);
            }
            if(delta_br.y()<=0)
            {
                s_g = true;
                to_move.setY(screen()->availableGeometry().bottomRight().y()-to_resize.height()+1);
            }
        }
        if(s_g) setGeometry(QRect(to_move,to_resize));
        else resize(to_resize);
        QString temp;
        if(to_resize == vidvid->sizeHint())
            temp = "original size (pix2pix)" +
                    QString::number(to_resize.width()) + "x" +
                    QString::number(to_resize.height());
        else
            temp = "size: " +
                    QString::number(to_resize.width()) + "x" +
                    QString::number(to_resize.height());
        simple_info_text(temp);
        return true;
    }
    else
    {
        QString temp("resolution barrier 144p ");
        simple_info_text(temp);
        return false;
    }
}

void Form::fullscreen ()
{
    if(animation->state() == QAbstractAnimation::State::Running) animation->stop();
    full = !full;
    if(full)this->showFullScreen();
    else this->showNormal();
}

void Form::progress_str(QString &format)
{
    ui->progressBar->setFormat(format);
}

void Form::file_str(QString &path)
{
    original_file_name = QFileInfo(path).fileName();
    ui->filename->setText(original_file_name);
    all_show(false);
}

void Form::simple_info_text(QString &txt)
{
    ui->filename->setText(txt);
    all_show(false);
}
