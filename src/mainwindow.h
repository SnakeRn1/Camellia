#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QMediaMetaData>
#include <QUrl>
#include <QFileDialog>
#include <iostream>
#include "form.h"
#include "inst.h"
#include <QSystemTrayIcon>
#include <QGraphicsEffect>
#include <QMenu>
#include <QMessageBox>
#include <QTimer>
#include <QListWidgetItem>
#include <QInputDialog>
#include <QProcess>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int index;
    int init_list_position;

private slots:
    void closeEvent(QCloseEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    void position_changed_slot(qint64 pos_now);
    void duration_changed_slot(qint64 dura_now);
    void meta_data_changed_slot();
    void state_changed_slot(QMediaPlayer::State state);
    void media_status_changed_slot(QMediaPlayer::MediaStatus status);
    void video_available_changed_slot(bool ava);
    void audio_available_changed_slot(bool ava);
    void video_window_escaped();
    void fwd();
    void bwd();
    void vol_slot(bool);
    void space();
    void open_next_file();
    void open_prev_file();
    void on_vol_valueChanged(int value);
    void on_video_show_button_clicked();
    void on_button_play_clicked();
    void on_button_stop_clicked();
    void on_button_close_clicked();
    void on_button_open_clicked();
    void on_top_stateChanged(int arg1);
    void on_tray_button_clicked();
    void trayIconActivated(QSystemTrayIcon::ActivationReason);
    void open_by_timer();
    void open_by_timer2();
    void on_rate_currentTextChanged(const QString &arg1);
    void on_files_list_customContextMenuRequested(const QPoint &pos);
    void slotRemoveRecord();
    void on_files_list_itemClicked(QListWidgetItem *item);
    void on_tube_button_clicked();

public slots:
    void open_new_file(QStringList);
    void try_to_open_file_by_index();
    void open_yt_link(QString input_str);
    void add_yt_link(QString input_str);

private:
    void open_by_index();
    void check_moving();
    QString getTimingQStr(int msf);
    QGraphicsDropShadowEffect *shadowEffect;
    QGraphicsDropShadowEffect *element_shading;
    Ui::MainWindow *ui;
    QMediaPlayer *m_player;
    Form* video_window;
    inst* inst_window;
    QString duration_str,position_str;
    qint64 dur_now_ms, pos_now_ms;
    QPoint d;
    QString file;
    QString yt_file;
    QString yt_str;
    QString last_opened_yt_link;
    QStringList list;
    bool moving_flag;
    QTimer* tm1;
    QTimer* tm2;
    QSystemTrayIcon *trayIcon;
    int muted_vol;
    QProcess file_addr_proc;
    QProcess file_name_proc;

signals:
    void exited();
};
#endif // MAINWINDOW_H
