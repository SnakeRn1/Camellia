#include "mainwindow.h"
#include <QApplication>
#include <QTextCodec>
#include <QSystemSemaphore>
#include <QSharedMemory>
#include <QStyleFactory>


int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);
    QSystemSemaphore semaphore("Camellia_Semaphore", 1);
    semaphore.acquire();
    QSharedMemory sharedMemory("Camellia_Shared_Memory");
    bool is_running;
    if (sharedMemory.attach())
    is_running = true;
    else
    {
        sharedMemory.create(1);
        is_running = false;
    }
    semaphore.release();
    if((is_running))
    {
        if(argc > 1)
        {
            QSettings set("SnakeR Soft","Camellia Media Player");
            QStringList ar = a.arguments();
            ar.removeFirst();
            set.setValue("to_open",ar);
            return 1;
        }
        if(argc == 1)
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText("Экземпляр Camellia Media Player уже запущен!\n"
                           "Закройте приложение и повторите попытку, либо\n"
                           "откройте требуемые файлы в запущенном ранее экземпляре.");
            msgBox.setWindowFlag(Qt::WindowType::WindowStaysOnTopHint);
            msgBox.exec();
            return 0;
        }
    }
    MainWindow w;
    QObject::connect(&w, &MainWindow::exited, &a, &QApplication::quit);
    if(argc > 1)
    {
        QStringList args = a.arguments();
        args.removeFirst();
        w.open_new_file(args);
    }
    else
    {
        QStringList ar;
        QSettings set("SnakeR Soft","Camellia Media Player");
        ar = set.value("last_files_list").toStringList();
        if(!ar.empty())
        {
            int initial_size = ar.size();
            QMutableListIterator<QString> i(ar);
            while (i.hasNext()) if (!QFile::exists(i.next())) i.remove();
            int opened_size = ar.size();
            if (opened_size == initial_size)
            {
                int ind = set.value("last_files_index").toInt();
                if((ind>0)&&(ind<ar.size())) w.init_list_position = ind;
            }
            if(!ar.empty()) w.open_new_file(ar);
        }
        else
        {
            QString yt_last_link = set.value("last_opened_yt_link").toString();
            if(!yt_last_link.isEmpty()) w.add_yt_link(yt_last_link);
        }
    }
    return a.exec();
}
