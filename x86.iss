#define MyAppName "Camellia Media Player x86"
#define MyAppVersion "1.4.3.0"
#define MyAppRegName "camellia_x86"
#define MyAppComName "SnakeR Soft"
#define MyAppExeName "camellia_x86.exe"

[Setup]
AppId={{1DF26818-2CAB-45C9-9994-8A74C24673FE}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
DefaultDirName={autopf}\{#MyAppName}
DisableDirPage=yes
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
InfoAfterFile=src\imgs\inst.txt
OutputDir=dst\SnakeR_Soft\Camellia_Media_Player
OutputBaseFilename=Camellia_Media_Player_x86_installer
SetupIconFile=src\imgs\ico.ico
Compression=lzma
SolidCompression=yes
WizardStyle=modern
UninstallDisplayIcon={app}/{#MyAppExeName}
UninstallDisplayName={#MyAppName}
ChangesAssociations=yes

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkablealone
Name: "sendtoicon"; Description: "������� ������ ���� '���������' ��� �������������� �������� ������"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkablealone

[Files]
Source: "{#MyAppRegName}\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#MyAppRegName}\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{usersendto}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: sendtoicon

[Registry]
Root: HKCU; Subkey: "Software\{#MyAppComName}"; Flags: dontcreatekey uninsdeletekey

Root: HKA; Subkey: "Software\{#MyAppComName}"; Flags: uninsdeletekeyifempty
Root: HKA; Subkey: "Software\{#MyAppComName}\{#MyAppName}"; Flags: uninsdeletekey
Root: HKA; Subkey: "Software\{#MyAppComName}\{#MyAppName}\Settings"; ValueType: string; ValueName: "Language"; ValueData: "{language}"

Root: HKA; Subkey: "Software\Classes\.mp3"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.mp3\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.mp3\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.mp3\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.mp3\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.mkv"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.mkv\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.mkv\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.mkv\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.mkv\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.flac"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.flac\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.flac\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.flac\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.flac\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.aac"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.aac\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.aac\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.aac\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.aac\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.ac3"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.ac3\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.ac3\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.ac3\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.ac3\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.m4a"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.m4a\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.m4a\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.m4a\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.m4a\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.mp4"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.mp4\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.mp4\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.mp4\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.mp4\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.avi"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.avi\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.avi\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.avi\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.avi\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.mov"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.mov\OpenWithProgids"; Flags: uninsdeletekeyifempty createvalueifdoesntexist
Root: HKA; Subkey: "Software\Classes\.mov\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.mov\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.mov\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.webm"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.webm\OpenWithProgids"; Flags: uninsdeletekeyifempty
Root: HKA; Subkey: "Software\Classes\.webm\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.webm\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.webm\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\.ogg"; ValueType: string; ValueName: ""; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.ogg\OpenWithProgids"; Flags: uninsdeletekeyifempty
Root: HKA; Subkey: "Software\Classes\.ogg\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppRegName}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\.ogg\OpenWithList"; Flags: uninsdeletekeyifempty dontcreatekey
Root: HKA; Subkey: "Software\Classes\.ogg\OpenWithList"; ValueType: string; ValueName: "a"; ValueData: "{#MyAppRegName}"; Flags: uninsdeletevalue dontcreatekey

Root: HKA; Subkey: "Software\Classes\{#MyAppRegName}"; ValueType: string; ValueName: ""; ValueData: "Camellia Media File"; Flags: uninsdeletekey
Root: HKA; Subkey: "Software\Classes\{#MyAppRegName}\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\{#MyAppExeName},0"
Root: HKA; Subkey: "Software\Classes\{#MyAppRegName}\shell\open"; ValueType: string; ValueName: "Icon"; ValueData: "{app}\{#MyAppExeName},0"
Root: HKA; Subkey: "Software\Classes\{#MyAppRegName}\shell\open"; ValueType: string; ValueName: "MUIVerb"; ValueData: "{#MyAppRegName}"
Root: HKA; Subkey: "Software\Classes\{#MyAppRegName}\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\{#MyAppExeName}""""%1"""

Root: HKA; Subkey: "Software\Classes\Folder\shell\{#MyAppRegName}"; Flags: uninsdeletekey
Root: HKA; Subkey: "Software\Classes\Folder\shell\{#MyAppRegName}"; ValueType: string; ValueName: "Icon"; ValueData: "{app}\{#MyAppExeName},0"
Root: HKA; Subkey: "Software\Classes\Folder\shell\{#MyAppRegName}"; ValueType: string; ValueName: "MUIVerb"; ValueData: "{#MyAppRegName}"
Root: HKA; Subkey: "Software\Classes\Folder\shell\{#MyAppRegName}\command"; ValueType: string; ValueName: ""; ValueData: """{app}\{#MyAppExeName}""""%1"""

Root: HKLM; Subkey: "Software\{#MyAppComName}"; Flags: uninsdeletekeyifempty; Check: IsAdminInstallMode
Root: HKLM; Subkey: "Software\{#MyAppComName}\{#MyAppName}"; Flags: uninsdeletekey; Check: IsAdminInstallMode
Root: HKLM; Subkey: "Software\{#MyAppComName}\{#MyAppName}\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Check: IsAdminInstallMode
Root: HKLM; Subkey: "Software\Microsoft\Windows\CurrentVersion\Explorer"; ValueType: dword; ValueName: "MultipleInvokePromptMinimum"; ValueData: "1"; Check: IsAdminInstallMode; Flags: uninsdeletevalue

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent